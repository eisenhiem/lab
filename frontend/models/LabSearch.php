<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lab;

/**
 * LabSearch represents the model behind the search form of `app\models\Lab`.
 */
class LabSearch extends Lab
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['labcode', 'lab_name', 'cgd', 'icd10tm', 'tmlt', 'tmlt_name', 'loinc', 'loinc_name', 'register_date', 'modified_date', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lab::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'register_date' => $this->register_date,
            'modified_date' => $this->modified_date,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'labcode', $this->labcode])
            ->andFilterWhere(['like', 'lab_name', $this->lab_name])
            ->andFilterWhere(['like', 'cgd', $this->cgd])
            ->andFilterWhere(['like', 'icd10tm', $this->icd10tm])
            ->andFilterWhere(['like', 'tmlt', $this->tmlt])
            ->andFilterWhere(['like', 'tmlt_name', $this->tmlt_name])
            ->andFilterWhere(['like', 'loinc', $this->loinc])
            ->andFilterWhere(['like', 'loinc_name', $this->loinc_name]);

        return $dataProvider;
    }
}
