<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\LabSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="lab-index">

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'panel'=>[
            'before'=>' '
            ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'labcode',
            'lab_name',
            'cgd',
            //'icd10tm',
            'tmlt',
            // 'tmlt_name',
            'loinc',
            // 'loinc_name',
            'register_date',
            'modified_date',
            //'d_update',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
