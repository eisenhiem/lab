<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LabSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lab-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'labcode') ?>

    <?= $form->field($model, 'lab_name') ?>

    <?= $form->field($model, 'cgd') ?>

    <?= $form->field($model, 'icd10tm') ?>

    <?php // echo $form->field($model, 'tmlt') ?>

    <?php // echo $form->field($model, 'tmlt_name') ?>

    <?php // echo $form->field($model, 'loinc') ?>

    <?php // echo $form->field($model, 'loinc_name') ?>

    <?php // echo $form->field($model, 'register_date') ?>

    <?php // echo $form->field($model, 'modified_date') ?>

    <?php // echo $form->field($model, 'd_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
