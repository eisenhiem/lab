<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Handles the creation of table `{{%lab}}`.
 */
class m200715_114948_create_lab_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lab}}', [
            'id' => $this->primaryKey(),
            'labcode' => Schema::TYPE_STRING.'(5)',
            'lab_name' => Schema::TYPE_STRING.' NOT NULL',
            'cgd' =>  Schema::TYPE_STRING.'(7)',
            'icd10tm' => Schema::TYPE_STRING.'(9)',
            'tmlt' => Schema::TYPE_STRING.'(10)',
            'tmlt_name' => Schema::TYPE_STRING,
            'loinc' => Schema::TYPE_STRING,
            'loinc_name' => Schema::TYPE_STRING,
            'register_date' => Schema::TYPE_DATE,
            'modified_date' => Schema::TYPE_DATE,
            'd_update' => Schema::TYPE_TIMESTAMP,
        ]);

        $this->execute(file_get_contents(__DIR__ . '/lab_standard.sql'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lab}}');
    }
}
