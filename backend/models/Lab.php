<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lab".
 *
 * @property int $id
 * @property string|null $labcode
 * @property string $lab_name
 * @property string|null $cgd
 * @property string|null $icd10tm
 * @property string|null $tmlt
 * @property string|null $tmlt_name
 * @property string|null $loinc
 * @property string|null $loinc_name
 * @property string|null $register_date
 * @property string|null $modified_date
 * @property string $d_update
 */
class Lab extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lab';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lab_name'], 'required'],
            [['register_date', 'modified_date', 'd_update'], 'safe'],
            [['labcode'], 'string', 'max' => 5],
            [['lab_name', 'tmlt_name', 'loinc', 'loinc_name'], 'string', 'max' => 255],
            [['cgd'], 'string', 'max' => 7],
            [['icd10tm'], 'string', 'max' => 9],
            [['tmlt'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'labcode' => 'รหัส LAB',
            'lab_name' => 'ชื่อ LAB',
            'cgd' => 'รหัส CGD',
            'icd10tm' => 'รหัส ICD10TM',
            'tmlt' => 'รหัส TMLT',
            'tmlt_name' => 'TMLT Name',
            'loinc' => 'รหัส Loinc',
            'loinc_name' => 'Loinc Name',
            'register_date' => 'วันที่ลงทะเบียน',
            'modified_date' => 'วันที่แก้ไข',
            'd_update' => 'วันที่ปรับปรุงข้อมูล',
        ];
    }
}
