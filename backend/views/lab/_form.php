<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Lab */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lab-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'labcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lab_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cgd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'icd10tm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tmlt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tmlt_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loinc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'loinc_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
